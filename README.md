# LabCOAT #



### What is LabCOAT? ###

**LabCOAT** (Laboratory **C**ontracts **O**rganized **A**s **T**echnology) is a decentralized scientific data ecosystem that consists of these synergistic components:

1. data DAO that represents research lab coalitions 
2. streaming data IoT platform 
3. data DEX marketplace for buying and selling curated datasets with access to compute pipelines for ML analysis 
4. LMS that integrates data, procedural guidelines, and regulatory compliance 
5. STEM-based social media network.